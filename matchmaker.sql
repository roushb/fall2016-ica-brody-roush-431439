CREATE DATABASE matchmaker;
CREATE TABLE `matchmaker`.`users` ( `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY , `name` VARCHAR(30) NOT NULL , `email` VARCHAR(50) NOT NULL , `pictureUrl` VARCHAR(255) NOT NULL , `description` TINYTEXT NOT NULL , `age` INT UNSIGNED NOT NULL , `posted` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , UNIQUE (`email`) ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'ica_user'@'localhost' IDENTIFIED BY 'ica_pass';
GRANT SELECT,INSERT,UPDATE,DELETE on matchmaker.* to ica_user@'localhost';
FLUSH privileges;
