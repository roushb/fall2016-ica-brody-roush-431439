<?php
require 'databaseAccess.php';
if(isset($_POST['createUser'])){
	//create username and escape input
	if(!empty($_POST['newUserName'])){
		$user = $mysqli->real_escape_string($_POST['newUserName']);
        }else{
			echo 'Error: Make sure to fill in a username.';
			#exit;
		}
	//create email and escape input
	if(!empty($_POST['email'])){
		$email = $mysqli->real_escape_string($_POST['email']);
       }else{
			echo 'Error: Make sure to fill in an email.';
			#exit;
	}
	//create age and escape input
	if(!empty($_POST['age'])){
		$age = (int) $_POST['age'];
       }else{
			echo 'Error: Make sure to fill in an email.';
			#exit;
	}
	//create description and escape input
	if(!empty($_POST['description'])){
		$description = $mysqli->real_escape_string($_POST['description']);
       }else{
			echo 'Error: Make sure to fill in a description.';
			#exit;
	}
	//create picture link and escape input
	if(!empty($_FILES['picture'])){
        $allowedExt = array('jpeg','jpg','png','gif');
        $file_name = $_FILES['picture']['name'];
		$explode = explode('.', $file_name);
		$end = end($explode);
        $file_ext = strtolower($end);
        $file_temp = $_FILES['picture']['tmp_name'];
        if(in_array($file_ext,$allowedExt)){
            //upload picture - use an md5 hash of the time to access the picture, ensuring that pictures don't have duplicate file names
            $file_path = 'uploads/'.substr(md5(time()), 0, 10) . '.' .$file_ext;
            move_uploaded_file($file_temp, $file_path);
        }else{
            echo 'Incorrect file type. Only PNG, jpeg, jpg, and gif are allowed.';
			#exit;
        }
       }else{
			echo 'Error: Make sure to upload a picture.';
			#exit;
	}
	//prepare and insert user into database
	$stmt = $mysqli->prepare("insert into users (name, email, age, description, pictureUrl ) values (?,?,?,?,?)");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}else{
	//bind user
        $bind = $stmt->bind_param('ssiss', $user, $email, $age, $description, $file_path);
        $execute = $stmt->execute();
        if(!$bind){
            printf("Bind Failed: %s\n", $mysqli->error);
            exit;
        }elseif(!$execute){
            printf("Execute Failed: %s\n", $mysqli->error);
            exit;
        }else{
            $stmt->close();
			header('Location: show-users.php');
        }    
    }
}




?>