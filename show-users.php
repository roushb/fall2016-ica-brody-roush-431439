<?php require 'databaseAccess.php' ?>
<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Matches</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
h1{
	text-align: center;
	width: inherit;
	text-decoration:underline;
}
p{
	text-align:center;	
}
img.prof-pic{
	max-width: 300px;	
}
</style>
</head>
<body><div id="main">
<h1>All Users</h1>
<form method="get" action="age-range.php" name="searchAge">
<table class="table">
 	<tr><td colspan=2>Search Age Range</td></tr>
    <tr><td>Min Age</td><td><input type="number" name = "low" min="18" class = "tField" /></td></tr>
    <tr><td>Max Age</td><td><input type="number" name = "high" min="18" class = "tField" /></td></tr>
    <tr><td colspan="2"><input type="submit" name="searchAge" class="signInSignUpButton" value="Search Age Range" /> </td></tr>
</table>
</form>
<div>
<table>
<?php
$stmt = $mysqli->prepare("select name, email, age, description, pictureUrl from users");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->execute();
$stmt->bind_result($name, $email, $age, $description, $pictureUrl);

while($stmt->fetch()){
		printf("<tr><td>Name:</td><td>%s</td><td>Email:</td><td>%s</td><td>Age:</td><td>%d</td>
		<td>Description:</td><td>%s</td><td>Image:</td><td><img src=\"%s\" alt=\"prof-pic\" class=\"prof-pic\" /></td></tr>",
		htmlspecialchars($name),
		htmlspecialchars($email),
		htmlspecialchars($age),
		htmlspecialchars($description),
		htmlspecialchars($pictureUrl));
}

$stmt->close();
    

?>
</table>
</div>
 
</div></body>
</html>