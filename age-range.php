<?php require 'databaseAccess.php'?>
<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Matchmaking Site - Users By Age</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
h1{
	text-align: center;
	width: inherit;
	text-decoration:underline;
}
p{
	text-align:center;	
}
img.prof-pic{
	max-width: 300px;	
}
</style>
</head>
<body><div id="main">
<h1>Users in Age Range</h1>
<table>
<?php
$low = (int) $_GET['low'];
$high = (int) $_GET['high'];
$stmt = $mysqli->prepare("select name, email, age, description, pictureUrl from users WHERE age >= '$low' AND age <= '$high'");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->execute();
$stmt->bind_result($name, $email, $age, $description, $pictureUrl);

while($stmt->fetch()){
	printf("<tr><td>Name:</td><td>%s</td><td>Email:</td><td>%s</td><td>Age:</td><td>%d</td>
	<td>Description:</td><td>%s</td><td>Image:</td><td><img src=\"%s\" alt=\"prof-pic\" class=\"prof-pic\" /></td></tr>",
	htmlspecialchars($name),
	htmlspecialchars($email),
	htmlspecialchars($age),
	htmlspecialchars($description),
	htmlspecialchars($pictureUrl));
}

$stmt->close();


?>
</table>
<div>
<a href="show-users.php">See all Users</a>
</div>
 
</div></body>
</html>